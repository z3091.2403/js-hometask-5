'use strict';
/*
    В этом задании нельзя менять разметку, только писать код в этом файле.
 */

/**
*   1. Удали со страницы элемент с id "deleteMe"
**/

function removeBlock() {
  document.getElementById('deleteMe').remove();
}

/**
 *  2. Сделай так, чтобы во всех элементах с классом wrapper остался только один параграф,
 *  в котором будет сумма чисел из всех параграфов.
 *
 *  Например, такой элемент:
 *
 *  <div class="wrapper"><p>5</p><p>15</p><p>25</p><p>35</p></div>
 *
 *  должен стать таким
 *
 *  <div class="wrapper"><p>80</p></div>
 */

function calcParagraphs() {
  const wrappers = document.querySelectorAll('.wrapper');
  for (let wrapper of wrappers) {
    let sum = 0;
    const paragraphs = wrapper.querySelectorAll('p');
    for (let paragraph of paragraphs) {
      sum += +paragraph.innerText;
      paragraph.remove();
    }
    let newPar = document.createElement('p');
    newPar.innerText = sum;
    wrapper.appendChild(newPar);
  }
}

/**
 *  3. Измени value и type у input c id "changeMe"
 *
 */

function changeInput() {
    let element = document.getElementById('changeMe');
    element.setAttribute('value', '123');
    element.type = 'number';
}

/**
 *  4. Используя функции appendChild и insertBefore дополни список с id "changeChild"
 *  чтобы получилась последовательность <li>0</li><li>1</li><li>2</li><li>3</li>
 *
 */

function appendList() {
  let list = document.getElementById('changeChild');
  let firstLi = document.createElement('li');
  firstLi.innerText = 1;
  list.insertBefore(firstLi, list.lastElementChild);
  let thirdLi = document.createElement('li');
  thirdLi.innerText = 3;
  list.appendChild(thirdLi);
}

/**
 *  5. Поменяй цвет всем div с class "item".
 *  Если у блока class "red", то замени его на "blue" и наоборот для блоков с class "blue"
 */

function changeColors() {
  const divCollection = document.querySelectorAll('.item');
  for (const div of divCollection) {
    if(div.className.includes('blue') || div.className.includes('red')) {
      div.classList.toggle('red');
      div.classList.toggle('blue');
    }
  }
}

removeBlock();
calcParagraphs();
changeInput();
appendList();
changeColors();
