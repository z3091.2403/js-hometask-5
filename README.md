# js-hometask-5

## Ответьте на вопросы

1. Какие данные хранятся в следующих свойствах узла: `parentElement`, `children`, `previousElementSibling` и `nextElementSibling`?
`parentElement` - родительский элемент выбранного элемента
`children` - вложенные узлы выбранного элемента, у которых тип ELEMENT_NODE
`previousElementSibling` - узел с типом ELEMENT_NODE, который идет перед выбранным элементом (не родительский, а соседский у общего родителя)
`nextElementSibling` - узел с типом ELEMENT_NODE, который идет после выбранного элемента (соседский у общего родителя, если нашл элемент не последний)
2. Нарисуй какое получится дерево для следующего HTML-блока.

```html
<p>Hello,<!--MyComment-->World!</p>
```
                                    <html>
                                    |
                                    p
                        |                |                          |  
                    Text "Hello,"   Comment "<!--MyComment-->"     Text "World!"  
## Выполните задания

1. Клонируй репозиторий;
2. Допиши функции в `tasks.js`;
3. Для проверки работы функций открой index.html в браузере;
4. Создай MR с решением.
